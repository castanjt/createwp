function copyFile(error, file) {
    if (error) {
        console.log(`CreateWP file write error. Check file and try again: ${file}`);
        process.exit(1);
    }
}

exports.copyFile = copyFile; 