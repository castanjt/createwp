const readline = require('readline-sync');
const term = require('terminal-kit').terminal;

exports.getUserOptions = () => {
    term.blue('Project details: \n')
    const composeProjectName = readline.question("Project Name [createwp]: ");
    const siteName = readline.question("Site Name [createwp]: ");
    const isHeadless = readline.keyInYN("Is this a headless project?", { guide: true });
    const migrationNeeded = readline.keyInYN("Do you need to pull down an existing site?", { guide: true });
    const themeNeeded = readline.keyInYN("Do you need to clone a theme repo?", { guide: true });

    term.blue('NGINX details: \n')
    const nginxPort = readline.question("NGINX Port [8050]: ", { limit: /^[0-9]{0,5}$/, limitMessage: "Please enter valid port." });

    term.blue('MySQL details: \n')
    const mysqlPort = readline.question("MySQL Port [3350]: ", { limit: /^[0-9]{0,5}$/, limitMessage: "Please enter valid port." });
    const mysqlRootPassword = readline.question("MySQL Root Password [createwp]: ", { hideEchoBack: true });
    const mysqlDatabase = readline.question("MySQL Database [createwp]: ");
    const mysqlUser = readline.question("MySQL User [createwp]: ");
    const mysqlPassword = readline.question("MySQL Password [createwp]:", { hideEchoBack: true });

    term.blue('WordPress details: \n')
    const wpPort = readline.question("WP Port [9000]: ", { limit: /^[0-9]{0,5}$/, limitMessage: "Please enter valid port." });
    const wpVersion = readline.question("WP Version [6.0.0]: ");
    const wpThemeRepo = (themeNeeded) ? readline.question("WP Theme Repo URL: [git@bitbucket.org:{some_org}/{some_project}.git] ") : '';
    const wpHeadlessUrl = (isHeadless) ? readline.question("WP Headless URL [http://localhost:3000]: ") : '';

    // Data migration setup
    if (migrationNeeded) {
        term.blue('Data migration details: \n')
    }
    const wpMigrateLicense = (migrationNeeded) ? readline.question("WP Migrate License: ") : '';
    const wpMigrateStagingEnv = (migrationNeeded) ? readline.question("WP Migrate Staging Env [https://staging-mysite.kinsta.cloud]: ") : '';
    const wpMigrateSecretKey = (migrationNeeded) ? readline.question("WP Migrate Secret Key: ", { hideEchoBack: true }) : '';

    const siteConfig = {
        "COMPOSE_PROJECT_NAME": composeProjectName || 'createwp',
        "SITE_NAME": siteName || 'createwp',
    }

    const nginxConfig = {
        "NGINX_PORT": nginxPort || 8050,
    }

    const wpConfig = {
        "WP_PORT": wpPort || 9000,
        "WP_VERSION": wpVersion || '6.0.0',
    }

    const dbConfig = {
        "MYSQL_PORT": mysqlPort || 3350,
        "MYSQL_ROOT_PASSWORD": mysqlRootPassword || 'createwp',
        "MYSQL_DATABASE": mysqlDatabase || 'createwp',
        "MYSQL_USER": mysqlUser || 'createwp',
        "MYSQL_PASSWORD": mysqlPassword || 'createwp',
    }

    const migrateConfig = {}

    if (isHeadless) {
        Object.assign(wpConfig, {
            "WP_HEADLESS_URL": wpHeadlessUrl || 'http://localhost:3000',
            "ACF_PRO_KEY": 'MWJlNjNiOTBlMGY0MWIzNjc4ODY1MTg4ZmQ3ZmEwYzAzNmE4ZTAzODIwMjFmMWVkZjMxMmNi',
        })
    }

    if (themeNeeded) {
        Object.assign(wpConfig, {
            "WP_THEME_REPO": wpThemeRepo || '',
        })
    }

    if (migrationNeeded) {
        Object.assign(migrateConfig, {
            "WP_MIGRATE_LICENSE": wpMigrateLicense || '5024b5a4-5b21-4a45-90bb-fe4d44992f3f',
            "WP_MIGRATE_STAGING_ENV": wpMigrateStagingEnv || '',
            "WP_MIGRATE_SECRET_KEY": wpMigrateSecretKey || '',
        })
    }

    return options = {
        env: {
            ...siteConfig,
            ...nginxConfig,
            ...wpConfig,
            ...dbConfig,
            ...migrateConfig            
        },
        config: {
            isHeadless: isHeadless || false,
            migrationNeeded: migrationNeeded || false,
            themeNeeded: themeNeeded || false
        } 
    };
}