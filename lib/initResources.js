const util = require('util');
const path = require('path');
const commandExists = require('command-exists');
const { exec } = require('child_process');
const term = require('terminal-kit').terminal;
const cwd = process.cwd();

exports.initResources = (themeRepo, projectName ) => {
      
    // Clone WP theme
    commandExists('git', function(err, commandExists) {
        if (commandExists && themeRepo) {
            const controller = new AbortController();
            const { signal } = controller;
            
            term.blue('Cloning WP Theme... \n')
            exec(`git clone ${themeRepo}`, { cwd: path.join(cwd, projectName, 'src', 'themes'), signal}, (error) => {
                if (error) {
                    term.red(`Problem with WP Theme! \n`)
                    console.error('WP Theme Error:', error)
                    process.exit(1)
                } else {
                    term.yellow(`Finished WP Theme! \n`)
                }
            })

        }
    })

    // Installing npm dependencies
    commandExists('npm', function(err, commandExists) {
        if (commandExists) {
            const controller = new AbortController();
            const { signal } = controller;

            term.blue(`Installing npm scripts... \n`)
            exec(`npm install`, { cwd: path.join(cwd, projectName), signal}, (error) => {
                if (error) {
                    term.red(`Problem with npm installation! \n`)
                    console.error('NPM install Error:', error)
                    process.exit(1)
                } else {
                    term.yellow(`Finished npm installation! \n`)
                }
            })
                
        }
    })
   
    // Launch VS Code
    commandExists('code', function(err, commandExists) {
        const controller = new AbortController();
        const { signal } = controller;

        if (commandExists) {
            exec(`code .`, { cwd: path.join(cwd, projectName), signal}, (error) => {
                if (error) {
                    term.red(`Problem launching VS Code! \n`)
                    console.error('VS Code Error:', error)
                    process.exit(1)
                }
            })

        }
    })  
}