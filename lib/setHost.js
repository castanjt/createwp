const hostile = require('hostile');
const yargs = require('yargs');

const argv = yargs
.option('site', {
    description: 'Domain for the hosts file',
    type: 'string'
}).argv;

const siteName = argv.site || 'createwp_env';

hostile.set('127.0.0.1', siteName + '.test', function(err) {
    if (err) {
        console.error(err);
        process.exit(1);
    } else {
        console.log('Host configured successfully!')
    }
})