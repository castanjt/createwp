const fs = require('fs');
const path = require('path');

exports.writeEnv = (cwd, projectName, env ) => {
    // Create .env in project directory
    const envBuilder = fs.createWriteStream(path.join(cwd, projectName, '.env'), { flags: 'a'})

    // Iterate over .env configuration and write values
    Object.keys(env).forEach(key => envBuilder.write(`${key}=${env[key]}\n`))
}