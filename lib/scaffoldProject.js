const fs = require('fs');
const path = require('path');
const term = require('terminal-kit').terminal;
const { copyFile } = require('../lib/utils');

exports.scaffoldProject = (cwd, projectName, config ) => {
    term.blue('Building CreateWP project... \n')

    // Scaffold directories
    fs.mkdirSync(path.join(cwd, projectName, 'stack', 'db-data'), {recursive: true});
    fs.mkdirSync(path.join(cwd, projectName, 'stack', 'nginx'), {recursive: true});
    fs.mkdirSync(path.join(cwd, projectName, 'stack', 'logs', 'nginx'), {recursive: true});
    fs.mkdirSync(path.join(cwd, projectName, 'stack', 'wordpress'), {recursive: true});
    fs.mkdirSync(path.join(cwd, projectName, 'stack', 'wp-cli'), {recursive: true});
    fs.mkdirSync(path.join(cwd, projectName, 'stack', 'composer'), {recursive: true});
    fs.mkdirSync(path.join(cwd, projectName, 'src', 'themes'), {recursive: true});
    fs.mkdirSync(path.join(cwd, projectName, 'src', 'plugins'), {recursive: true});

    // Copy project assets
    fs.copyFile(__dirname + '/../assets/config/project/docker-compose.yml', path.join(cwd, projectName, 'docker-compose.yml'), (err) => copyFile(err, 'docker-compose.yml'));
    fs.copyFile(__dirname + '/../assets/config/project/package.json', path.join(cwd, projectName, 'package.json'), (err) => copyFile(err, 'package.json'));
    fs.copyFile(__dirname + '/../assets/config/project/README.md', path.join(cwd, projectName, 'README.md'), (err) => copyFile(err, 'README.md'));

    // Copy nginx config
    fs.copyFile(__dirname + '/../assets/config/nginx/wordpress.conf', path.join(cwd, projectName, 'stack', 'nginx', 'wordpress.conf'), (err) => copyFile(err, 'wordpress.conf'));

    // Copy WP CLI scripts
    fs.copyFile(__dirname + '/../assets/wp-cli/db-migration.sh', path.join(cwd, projectName, 'stack', 'wp-cli', 'db-migration.sh'), (err) => copyFile(err, 'db-migration.sh'));
    fs.copyFile(__dirname + '/../assets/wp-cli/setup.sh', path.join(cwd, projectName, 'stack', 'wp-cli', 'setup.sh'), (err) => copyFile(err, 'setup.sh'));
   
    // Copy composer config 
    fs.copyFile(__dirname + '/../assets/config/composer/auth.json', path.join(cwd, projectName, 'stack', 'composer', 'auth.json'), (err) => copyFile(err, 'auth.json'));
    fs.copyFile(__dirname + '/../assets/config/composer/composer-config.sh', path.join(cwd, projectName, 'stack', 'composer', 'composer-config.sh'), (err) => copyFile(err, 'composer-config.sh'));

    if (config.isHeadless) {
        fs.copyFile(__dirname + '/../assets/config/composer/composer-headless.json', path.join(cwd, projectName, 'stack', 'composer', 'composer.json'), (err) => copyFile(err, 'composer-headless.json'));
    } else {
        fs.copyFile(__dirname + '/../assets/config/composer/composer-standard.json', path.join(cwd, projectName, 'stack', 'composer', 'composer.json'), (err) => copyFile(err, 'composer-standard.json'));
    }
}