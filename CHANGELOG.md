# Changelog

## [4.1.1] - 2022-06-28

### Release Notes:

- Adds wp-graphql-gutenberg back to the project and locks version
## [4.1.0] - 2022-06-23

### Release Notes:

- Provides better tooling around custom git repo themes
- Prevents composer from overwritting custom git repo theme
- Installs boilerplate headless theme, if no theme specified within headless projects

## [4.0.3] - 2022-06-07

### Release Notes:

- ACF Pro support

## [4.0.2] - 2022-06-07

### Release Notes:

- Updated documentation

## [4.0.1] - 2022-06-07

### Release Notes:

- Adds activiation for wp-headless-theme on headless setup
- Locks wp-graphql-gutenberg version due to bugs in package

## [4.0.0] - 2022-06-03

### Release Notes:

- Major upgrade to support headless builds
- Removes wp-migrate-db-pro from repo
- Adds Composer to Docker
- Utilizes Composer configs for different build types
- Cleans script up to be more modular

## [3.5.0] - 2022-05-09

### Release Notes:

- Added validation to prompts
- Moved logic to initResources file 
- Renamed sudo process

## [3.4.1] - 2022-05-06

### Release Notes:

- Adds migration script to README.md

## [3.4.0] - 2022-05-06

### Release Notes:

- Adds functionality to write to hosts file
- Requires project option for init
- Adds better console logging and prompts
- Adds terminal-kit to project
- Logs local host address
- Installs npm dependencies within created project
- Updated CLI README.md documentation
- Adds README.md documentation for project folder
- Update description in package.json for project folder

## [3.3.0] - 2022-05-05

### Release Notes:

- Adds hostile package to write to /etc/hosts
- Adds CHANGELOG.md

## [3.2.2] - 2022-05-04

### Release Notes:

- Fixes script error in WP CLI migration script

## [3.2.1] - 2022-05-04

### Release Notes:

- Adds Docker up command
- Fixes typo in CLI prompts

## [3.2.0] - 2022-05-04

### Release Notes:

- Adds setup scripts and ability to remote into Docker to execute them

## [3.1.0] - 2022-05-04

### Release Notes:

- Updates Migrate DB version
- Adds WP CLI container
- Adds WP CLI migration script
- Support for command checks, Migrate DB config, and theme repo variables

## [3.0.1] - 2022-05-04

### Release Notes:

- Added Migrate DB Pro plugins
- Removed unnecessary assets and scripts
- Added README.md
- Implements Node.JS instead of Bash

## [3.0.0] - 2022-05-03

### Release Notes:

- Rebuilds CreateWP using NGINX instead of Apache
- NodeJS now is installed as a global package
- See previous git tags for Apache version