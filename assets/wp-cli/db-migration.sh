#!/bin/sh
wp migrate setting update license ${WP_MIGRATE_LICENSE} --user=1
wp migrate pull ${WP_MIGRATE_STAGING_ENV} ${WP_MIGRATE_SECRET_KEY} \
    --plugin-files=all \
    --media=all