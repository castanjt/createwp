#!/bin/sh
wp core install \
    --url=${SITE_NAME}.test:${NGINX_PORT} \
    --title=${SITE_NAME} \
    --admin_user=${MYSQL_USER} \
    --admin_password=${MYSQL_PASSWORD} \
    --admin_email=jcastanza@fca.org
wp plugin delete akismet hello
wp plugin activate --all

if [[ ! -z ${WP_HEADLESS_URL} ]]
then
    wp config set HEADLESS_FRONTEND_URL ${WP_HEADLESS_URL} --add
    wp config set PREVIEW_SECRET_TOKEN '2d1ffb1e09c5781c48970965ed0b73d2' --add
    wp config set GRAPHQL_JWT_AUTH_SECRET_KEY 'a79383cdbb367504b91dcf8c103f53ab' --add
fi

if [[ -z ${WP_THEME_REPO} ]]
then
    wp theme activate wp-headless-theme
fi 
    