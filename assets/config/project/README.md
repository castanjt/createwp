# CreateWP Project

CreateWP Project provides a means of doing WordPress Theme and Plugin development.

## Folder Structure

CreateWP configures the following folder structure:

```
└── stack
    ├── composer
    │   └── auth.json
    │   └── composer.json
    │   └── composer-config.sh
    ├── db-data
    ├── logs
    │   └── nginx
    ├── nginx
    │   └── wordpress.conf
    ├── wordpress
    └── wp-cli
        ├── db-migration.sh
        └── setup.sh
├── src
│   ├── plugins
│   ├── themes
```

The `stack` folder contains a series of bind mounts that will allow Docker to connect its container directories to your local file system when it installs MariaDB, NGINX, WordPress, Composer, and WP-CLI. These folders will contain your database files, logs, NGINX configuration, Composer configuration, WordPress core, and WP-CLI scripts. Modifying these files may break your environment. Please use care and avoid modifying them, unless necessary.

The `src` folder creates bind mounts for your themes and plugins. This allows you to clone or create a `.git` repository into this folder and have it instantly pulled into the container and ready to use on a WordPress site. Alternatively, Composer may install your theme as a dependency. These folders map directly to the `wp-content/plugins` and `wp-content/themes` directories. 

## Start Docker

To start Docker, you will need Docker installed on your local machine. To build containers, simply run: `npm run docker:up`

## Configuring WordPress

After Docker is fully running, you can run the following script: `npm run wp:setup`.

This command will install Composer dependencies, then run WP-CLI in order to install WordPress, delete un-needed plugins, and activate plugins.

## Migrating Databases

During initial setup, if you provided values for:

```
    WP_MIGRATE_LICENSE
    WP_MIGRATE_STAGING_ENV
    WP_MIGRATE_SECRET_KEY
```

you should be ready to migrate a staging database to your local instance. If not, you'll want to update your `.env` file and restart your containers with the correct values.

The `WP_MIGRATE_SECRET_KEY` can be found within the staging instance that you're trying to migrate. You'll need to login to that instance to get the key.

When migrating, the migration script is setup by default to migrate database, media, and plugins. 

To run the initial migration: `npm run wp:migrate`.

For subsquent migrations, please be sure to Docker Exec into the WP-CLI container and run the appropriate `wp` commands rather than re-running the migration script.

For more information, please visit: 

- https://developer.wordpress.org/cli/commands/
- https://deliciousbrains.com/wp-migrate-db-pro/docs/cli/


## License
[MIT](https://choosealicense.com/licenses/mit/)