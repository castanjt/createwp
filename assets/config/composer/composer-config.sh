#!/bin/sh
composer install

if [[ -z ${WP_THEME_REPO} ]]
then
    composer require castanjt/wp-headless-theme
fi 