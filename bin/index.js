#! /usr/bin/env node

const yargs = require('yargs');
const term = require('terminal-kit').terminal;
const { spawn } = require('child_process');
const { initResources } = require('../lib/initResources');
const { scaffoldProject } = require('../lib/scaffoldProject');
const { writeEnv } = require('../lib/writeEnv');
const { getUserOptions } = require('../lib/getUserOptions');

const argv = yargs
.option('project', {
    alias: 'p',
    description: 'Creates a directory with the name of your project.',
    type: 'string',
})
.demandOption('project', 'Please provide a project name.')
.help()
.alias('help', 'h').argv;

const cwd = process.cwd();
const projectName = argv.project || 'createwp_env';

// Set introduction prompt
term.bold.underline.yellow('Welcome to CreateWP. \n')
term.yellow(`Let's setup your project: \n`)
term.blue(`Type an .env value OR press Enter to use the default value in brackets. \n`)
term.red(`Ctrl C to exit. \n`)

const options = getUserOptions();

scaffoldProject(cwd, projectName, options.config);
writeEnv(cwd, projectName, options.env);

// Set ENV prompt
term.yellow('Configuration success! \n')
term.blue(`Setting up your /etc/hosts configuration. Please type your root password. \n`)

// Write hosts config
const controller = new AbortController();
const { signal } = controller;
const sudoProcess = spawn('sudo', ['node', __dirname + '/../lib/setHost.js', '--site', options.env.SITE_NAME], { signal });
sudoProcess.on('error', (err) => {
    term.red(`Problem with /etc/hosts writing... \n`)
    console.error('Host Write Error:', err)
});

sudoProcess.on('exit', () => {
    term.yellow(`Hosts success! \n`)
    term.blue(`Site domain: http://${options.env.SITE_NAME}.test:${options.env.NGINX_PORT} \n`)
    
    initResources( options.env.WP_THEME_REPO, projectName );
    term.yellow("Finished setup! \n")
});



