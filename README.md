# CreateWP CLI

CreateWP CLI is a cli tool to assist with spinning up WordPress environments for standard and headless setup running NGINX, MariaDB, Composer, and WP CLI.

## CLI Configuration

To install, you may need to modify your `.npmrc` file in order to setup the private repo:

Run: `aws codeartifact login --tool npm --repository castanjt-npm-packages --domain castanjt --domain-owner 764751313814 --namespace @castanjt`

Then update your `~/.npmrc` file to add the main npm registry, if the value is overwritten. Your `~/.npmrc` file should look something like this:

```npm
registry=https://registry.npmjs.org/
@castanjt:registry=https://castanjt-764751313814.d.codeartifact.us-east-1.amazonaws.com/npm/castanjt-npm-packages/...
```

## CLI Installation

```bash
npm install @castanjt/createwp -g
```

## CLI Updating

```bash
npm update @castanjt/createwp -g
```

## CLI Usage

Creating a new CreateWP project/stack:

```npm
createwp -p ${project_name}
```
Viewing help options:

```npm
createwp -h
```

Viewing current version:

```npm
createwp --version
```

CreateWP will generate a project/stack folder in the current working directory where the command is executed.
## Configurating a CreateWP project/stack
The CLI will prompt you for specific values for your `.env` file. These will need to be different, if running multiple instances of CreateWP.

The CLI will walk you through configuring these `.env` values for your project:

```
    COMPOSE_PROJECT_NAME
    SITE_NAME
    NGINX_PORT
    WP_PORT
    WP_VERSION
    WP_THEME_REPO
    WP_HEADLESS_URL
    MYSQL_PORT
    MYSQL_ROOT_PASSWORD
    MYSQL_DATABASE
    MYSQL_USER
    MYSQL_PASSWORD
    WP_MIGRATE_LICENSE
    WP_MIGRATE_STAGING_ENV
    WP_MIGRATE_SECRET_KEY
```

* NOTE: Some `.env` values are dynamic based upon whether your select a data migration or headless setup. If running multiple CreateWP projects, you will need to use unique port values and names for each CreateWP project instance.

* HELPFUL TIP: Get your `WP_THEME_REPO` and `WP_MIGRATE_SECRET_KEY` values before running setup, if doing non-headless project.

## Automated tasks

- The CLI will automatically configure your `/etc/hosts` setting and generate your WordPress URL using `SITE_NAME` and `NGINX_PORT`.
    - e.g. `http://${SITE_NAME}.test:${NGINX_PORT}` is where your site will be found after setup is complete and Docker is started.

- The CLI will also clone your WordPress theme, if `WP_THEME_REPO` value is a valid git repository (e.g. `git@bitbucket.org:castanjt:{some_repo}.git` )
- The CLI will automatically install npm dependencies for your created project.

## License
[MIT](https://choosealicense.com/licenses/mit/)